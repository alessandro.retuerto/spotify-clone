import { Artist } from './artist.interface';

export interface ArtistState {
  loading: boolean;
  artist: ReadonlyArray<Artist>;
}
