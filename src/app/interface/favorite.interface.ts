export interface Favorite {
  image: string;
  song: string;
  album: string;
  id: string;
  isSelect: boolean;
}
