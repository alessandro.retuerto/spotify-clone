import { Item } from './album.interface';

export interface ItemState {
  loading: boolean;
  item: ReadonlyArray<Item>;
}
