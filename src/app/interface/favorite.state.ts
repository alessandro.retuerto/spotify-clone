import { Favorite } from './favorite.interface';

export interface FavoriteState {
  loading: boolean;
  favorite: ReadonlyArray<Favorite>;
}
