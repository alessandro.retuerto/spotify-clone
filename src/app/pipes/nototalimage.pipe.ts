import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nototalimage',
})
export class NototalimagePipe implements PipeTransform {
  transform(artist: any): any {
    if (artist.followers?.total > 0) {
      return artist.followers.total;
    } else {
      return 'No total';
    }
  }
}
