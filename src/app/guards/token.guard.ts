import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { SpotifyService } from '../services/spotify.service';

@Injectable({
  providedIn: 'root',
})
export class TokenGuard implements CanActivate {
  estado: boolean = true;
  constructor(
    private router: Router,
    private spotify: SpotifyService,
    private cookieService: CookieService
  ) {}

  canActivate():
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (window.location.hash) {
      this.spotify.splitToken();
    }
    if (this.cookieService.get('token')) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
