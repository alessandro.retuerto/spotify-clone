import { TokenGuard } from './token.guard';

import { TestBed, getTestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CookieService } from 'ngx-cookie-service';
import { SpotifyService } from '../services/spotify.service';
import { Router } from '@angular/router';

describe('TokenGuard', () => {
  let guard: TokenGuard;
  let spotifyService: SpotifyService;
  let cookie: CookieService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule],
      providers: [TokenGuard, CookieService, SpotifyService],
    });
    guard = TestBed.inject(TokenGuard);
    cookie = TestBed.inject(CookieService);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
