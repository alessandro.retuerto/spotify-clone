import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { ItemState } from '../../interface/item.state';

export const selectItemFeature = (state: AppState) => state.item;

export const selectListItem = createSelector(
  selectItemFeature,
  (state: ItemState) => state.item
);

export const selectLoading = createSelector(
  selectItemFeature,
  (state: ItemState) => state.loading
);
