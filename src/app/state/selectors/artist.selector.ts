import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { ItemState } from '../../interface/item.state';
import { ArtistState } from '../../interface/artistst.state';

export const selectArtistFeature = (state: AppState) => state.artist;

export const selectListArtist = createSelector(
  selectArtistFeature,
  (state: ArtistState) => state.artist
);

export const selectLoading = createSelector(
  selectArtistFeature,
  (state: ArtistState) => state.loading
);
