import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { FavoriteState } from '../../interface/favorite.state';

export const selectFavoriteFeature = (state: AppState) => state.favorite;

export const selectListFavorite = createSelector(
  selectFavoriteFeature,
  (state: FavoriteState) => state.favorite
);

export const selectLoading = createSelector(
  selectFavoriteFeature,
  (state: FavoriteState) => state.loading
);
