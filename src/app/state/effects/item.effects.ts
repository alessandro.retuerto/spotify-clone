import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { SpotifyService } from '../../services/spotify.service';

@Injectable()
export class ItemsEffects {
  loadItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Item List] Load item'),
      mergeMap(() =>
        this.spotifyService.getNewReleases().pipe(
          map((item) => ({
            type: '[Item List] Loaded succes',
            item: item.albums.items,
          })),
          catchError(() => EMPTY)
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private spotifyService: SpotifyService
  ) {}
}
