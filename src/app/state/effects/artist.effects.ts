import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { SpotifyService } from '../../services/spotify.service';
import {
  loadedArtist,
  loadedArtistId,
  loadArtist,
} from '../actions/artist.actions';

@Injectable()
export class ArtistEffects {
  loadArtist$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadedArtistId),
      mergeMap((action) =>
        this.spotifyService.getArtists(action.parameters).pipe(
          map((artist) => ({
            type: '[Artist List] Loaded succes',
            artist: artist.artists.items,
          })),
          catchError(() => EMPTY)
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private spotifyService: SpotifyService
  ) {}
}
