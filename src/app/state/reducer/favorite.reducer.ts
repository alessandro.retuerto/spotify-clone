import { createReducer, on } from '@ngrx/store';
import {
  loadFavorites,
  loadedFavorites,
  addFavorites,
} from '../actions/favorite.actions';
import { FavoriteState } from '../../interface/favorite.state';
import { removeFavorite } from '../actions/favorite.actions';

export const initialState: FavoriteState = { loading: false, favorite: [] };

export const favoriteReducer = createReducer(
  initialState,
  on(loadFavorites, (state) => {
    return { ...state, loading: true };
  }),
  on(loadedFavorites, (state, { favorite }) => {
    return { ...state, loading: false, favorite };
  }),
  on(addFavorites, (state, favorite) => ({
    ...state,
    favorite: [...state.favorite, favorite.favorite],
  })),
  on(removeFavorite, (state, payload) => ({
    ...state,
    favorite: state.favorite.filter(
      (favorite) => favorite.id != payload.favorite.id
    ),
  }))
);
