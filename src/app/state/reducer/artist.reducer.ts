import { createReducer, on } from '@ngrx/store';
import { ArtistState } from '../../interface/artistst.state';
import {
  loadedArtist,
  loadArtist,
  loadedArtistId,
  loadArtistSearch,
} from './../actions/artist.actions';

export const initialState: ArtistState = { loading: false, artist: [] };

export const artistReducer = createReducer(
  initialState,
  on(loadArtist, (state) => {
    return { ...state, loading: true };
  }),
  on(loadedArtist, (state, { artist }) => {
    return { ...state, loading: false, artist };
  }),
  on(loadedArtistId, (state, { parameters }) => {
    return { ...state, loading: false, parameters };
  }),
  on(loadArtistSearch, (state) => {
    return { ...state, loading: false };
  })
);
