import { createReducer, on } from '@ngrx/store';
import { ItemState } from '../../../../src/app/interface/item.state';
import { loadedItem, loadItem } from '../actions/item.actions';

export const initialState: ItemState = { loading: false, item: [] };

export const itemReducer = createReducer(
  initialState,
  on(loadItem, (state) => {
    return { ...state, loading: true };
  }),
  on(loadedItem, (state, { item }) => {
    return { ...state, loading: false, item };
  })
);
