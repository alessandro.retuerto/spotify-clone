import { ActionReducerMap } from '@ngrx/store';
import { ItemState } from '../interface/item.state';
import { itemReducer } from './reducer/item.reducer';
import { ArtistState } from '../interface/artistst.state';
import { artistReducer } from './reducer/artist.reducer';
import { FavoriteState } from '../interface/favorite.state';
import { favoriteReducer } from './reducer/favorite.reducer';

export interface AppState {
  item: ItemState;
  artist: ArtistState;
  favorite: FavoriteState;
}

export const ROOT_REDUCER: ActionReducerMap<AppState> = {
  item: itemReducer,
  artist: artistReducer,
  favorite: favoriteReducer,
};
