import { createAction, props } from '@ngrx/store';
import { Favorite } from '../../interface/favorite.interface';

export const loadFavorites = createAction('[Favorite List] Load favorite');

export const loadedFavorites = createAction(
  '[Favorite List] Loaded success',
  props<{ favorite: Favorite[] }>()
);
export const addFavorites = createAction(
  '[Favorite Add] Add success',
  props<{ favorite: Favorite }>()
);
export const removeFavorite = createAction(
  '[Favorite Remove] Remove success',
  props<{ favorite: Favorite }>()
);
