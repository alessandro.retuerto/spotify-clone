import { Artist } from './../../interface/artist.interface';
import { createAction, props } from '@ngrx/store';

export const loadArtist = createAction('[Artist List] Load artist');

export const loadedArtist = createAction(
  '[Artist List] Loaded succes',
  props<{ artist: Artist[] }>()
);

export const loadedArtistId = createAction(
  '[Artist List id] Loaded succes id',
  props<{ parameters: string }>()
);

export const loadArtistSearch = createAction(
  '[Artist List] Load artist search'
);
