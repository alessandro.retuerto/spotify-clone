import { createAction, props } from '@ngrx/store';

import { Item } from '../../interface/album.interface';

export const loadItem = createAction('[Item List] Load item');

export const loadedItem = createAction(
  '[Item List] Loaded succes',
  props<{ item: Item[] }>()
);
