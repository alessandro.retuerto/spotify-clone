export const favorite = [
  {
    album: 'Hybrid Theory (Bonus Edition)',
    id: '60a0Rd6pjrkxjPbaKzXjfq',
    image: 'https://i.scdn.co/image/ab67616d0000b273e2f039481babe23658fc719a',
    isSelect: true,
    song: 'In the End',
  },
  {
    album: 'Meteora',
    id: '2nLtzopw4rPReszdYBJU6h',
    image: 'https://i.scdn.co/image/ab67616d0000b273b4ad7ebaf4575f120eb3f193',
    isSelect: true,
    song: 'Numb',
  },
  {
    album: 'Minutes to Midnight',
    id: '18lR4BzEs7e3qzc0KVkTpU',
    image: 'https://i.scdn.co/image/ab67616d0000b27346e207de66ba06422897f769',
    isSelect: true,
    song: 'What I ve Done',
  },
];
