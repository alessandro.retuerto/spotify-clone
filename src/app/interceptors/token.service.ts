import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root',
})
export class TokenService implements HttpInterceptor {
  constructor(private cookieService: CookieService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let cifr = this.cookieService.get('token');
    let token = CryptoJS.AES.decrypt(cifr, 'token').toString(CryptoJS.enc.Utf8);

    const headers = new HttpHeaders({
      Authorization: `Bearer ${token}`,
    });

    const reqClone = req.clone({
      headers,
    });

    return next.handle(reqClone);
  }
}
