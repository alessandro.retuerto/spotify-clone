import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Album } from '../interface/album.interface';
import { ArtistColection } from '../interface/artist.interface';
import { CookieService } from 'ngx-cookie-service';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root',
})
export class SpotifyService {
  constructor(private http: HttpClient, private cookieService: CookieService) {}

  audio = new Audio();
  musicInit: any = {};
  isSelectMusic: boolean = false;

  getNewReleases() {
    return this.http.get<Album>(
      'https://api.spotify.com/v1/browse/new-releases?limit=20'
    );
  }

  getArtists(term: string) {
    return this.http.get<ArtistColection>(
      `https://api.spotify.com/v1/search?q=${term}&type=artist&limit=20`
    );
  }

  getArtist(id: string) {
    return this.http.get(`https://api.spotify.com/v1/artists/${id}`);
  }
  getTopTracks(id: string) {
    return this.http.get(
      `https://api.spotify.com/v1/artists/${id}/top-tracks?market=us`
    );
  }

  splitToken() {
    const token = window.location.hash
      .substring(1)
      .split('&')
      .reduce((prev: any, curr) => {
        const parts: any[] = curr.split('=');
        prev[parts[0]] = decodeURIComponent(parts[1]);
        return prev;
      }, {});
    var ciphertext = CryptoJS.AES.encrypt(
      token.access_token,
      'token'
    ).toString();

    this.cookieService.set('token', ciphertext, { expires: 1 });
    window.location.hash = '';
  }
}
