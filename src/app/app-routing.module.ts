import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { FavoritesComponent } from './pages/favorites/favorites.component';
import { SearchComponent } from './pages/search/search.component';
import { LoginComponent } from './pages/login/login.component';
import { TokenGuard } from './guards/token.guard';
import { ArtistComponent } from './pages/artist/artist.component';
import { LoginGuard } from './guards/login.guard';
import { IndexComponent } from './pages/index/index.component';

export const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    canActivate: [LoginGuard],
  },

  {
    path: '',
    component: IndexComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
        canActivate: [TokenGuard],
      },

      {
        path: 'search',
        component: SearchComponent,
        canActivate: [TokenGuard],
      },
      {
        path: 'favorites',
        component: FavoritesComponent,
        canActivate: [TokenGuard],
      },
      {
        path: 'artist/:id',
        component: ArtistComponent,

        canActivate: [TokenGuard],
      },
    ],
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoginGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
