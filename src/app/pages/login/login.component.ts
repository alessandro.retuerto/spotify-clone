import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(private service: SpotifyService, private route: Router) {}

  ngOnInit(): void {
    console.log(environment);
  }

  authURL() {
    const scopes = [
      'user-read-currently-playing',
      'user-read-recently-played',
      'user-top-read',
      'user-modify-playback-state',
    ];
    /*    const redirectURI = 'http://127.0.0.1:4200/home'; */

    const redirectURI = environment.redirectURI;

    const url = `https://accounts.spotify.com/authorize/?client_id=${
      environment.client_id
    }&response_type=token&redirect_uri=${redirectURI}&scope=${scopes.join(
      '%20'
    )}&show_dialog=true`;
    return url;
  }
}
