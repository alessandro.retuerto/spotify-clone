import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
})
export class FavoritesComponent implements OnInit {
  isSelectMusic: boolean = false;
  constructor(private spotify: SpotifyService) {}

  ngOnInit(): void {
    this.isSelectMusic = this.spotify.isSelectMusic;
  }
}
