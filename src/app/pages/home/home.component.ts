import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadItem } from '../../../../src/app/state/actions/item.actions';
import { Observable } from 'rxjs';
import { selectLoading } from '../../state/selectors/item.selector';
import { SpotifyService } from '../../services/spotify.service';

import * as CryptoJS from 'crypto-js';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  estatusLoading: boolean = false;
  isControls: boolean = true;
  isLoading$: Observable<boolean> = new Observable();

  constructor(
    private store: Store<any>,
    private spotify: SpotifyService,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.isControls = this.spotify.isSelectMusic;
    this.isLoading$ = this.store.select(selectLoading);
    this.store.dispatch(loadItem());
  }
}
