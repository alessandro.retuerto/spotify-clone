import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/Observable';
import {
  loadArtist,
  loadedArtistId,
} from '../../../../src/app/state/actions/artist.actions';
import { selectLoading } from '../../../../src/app/state/selectors/artist.selector';
import { selectListArtist } from '../../state/selectors/artist.selector';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  isLoading$: Observable<boolean> = new Observable();
  artist$: Observable<any> = new Observable();
  isArtist: boolean = false;
  isSelectMusic: boolean = false;

  constructor(private store: Store<any>, private spotify: SpotifyService) {}

  ngOnInit(): void {
    this.store.dispatch(loadArtist());
    this.isSelectMusic = this.spotify.isSelectMusic;
    this.store.select(selectLoading).subscribe((data) => {
      this.isArtist = data;
    });
    this.artist$ = this.store.select(selectListArtist);

    this.store.dispatch(loadedArtistId({ parameters: 'Linkin Park' }));
  }
}
