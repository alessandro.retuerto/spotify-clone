import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { AtomsModule } from '../components/atoms/atoms.module';
import { MoleculesModule } from '../components/molecules/molecules.module';
import { RouterModule } from '@angular/router';
import { OrganismsModule } from '../components/organisms/organisms.module';
import { LoginComponent } from './login/login.component';
import { SearchComponent } from './search/search.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { NoimagePipe } from '../pipes/noimage.pipe';
import { ArtistComponent } from './artist/artist.component';
import { SharedModule } from '../components/shared/shared.module';
import { NototalimagePipe } from '../pipes/nototalimage.pipe';
import { IndexComponent } from './index/index.component';
import { TemplateModule } from '../components/template/template.module';

@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent,
    SearchComponent,
    FavoritesComponent,
    NoimagePipe,
    ArtistComponent,
    NototalimagePipe,
    IndexComponent,
  ],
  imports: [
    CommonModule,
    AtomsModule,
    MoleculesModule,
    OrganismsModule,
    RouterModule,
    SharedModule,
    TemplateModule,
  ],
  exports: [HomeComponent, IndexComponent],
})
export class PagesModule {}
