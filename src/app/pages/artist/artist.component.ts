import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss'],
})
export class ArtistComponent implements OnInit {
  artist: any = {};
  isSelectMusic: boolean = false;

  constructor(private rauter: ActivatedRoute, private spotify: SpotifyService) {
    this.rauter.params.subscribe((params: any) => {
      this.getArtist(params.id);
    });
  }

  ngOnInit(): void {
    this.isSelectMusic = this.spotify.isSelectMusic;
  }

  getArtist(id: string) {
    this.spotify.getArtist(id).subscribe({
      next: (data: any) => {
        this.artist = data;
      },
      error: (error) => {
        console.log(error);
      },
    });
  }
  ngDoCheck(): void {
    this.isSelectMusic = this.spotify.isSelectMusic;
  }
}
