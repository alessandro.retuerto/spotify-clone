import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoleculesModule } from '../molecules/molecules.module';
import { AppRoutingModule } from '../../../../src/app/app-routing.module';
import { ListCardMusicComponent } from './list-card-music/list-card-music.component';
import { SharedModule } from '../shared/shared.module';
import { ListCardSearchComponent } from './list-card-search/list-card-search.component';
import { NoimagePipeSearch } from '../../pipes/noimageSearch.pipe';
import { ListCardArtistComponent } from './list-card-artist/list-card-artist.component';
import { ListCardFavoriteComponent } from './list-card-favorite/list-card-favorite.component';
@NgModule({
  declarations: [
    ListCardMusicComponent,
    ListCardSearchComponent,
    NoimagePipeSearch,
    ListCardArtistComponent,
    ListCardFavoriteComponent,
  ],
  imports: [CommonModule, MoleculesModule, AppRoutingModule, SharedModule],
  exports: [
    ListCardMusicComponent,
    ListCardSearchComponent,
    ListCardArtistComponent,
    ListCardFavoriteComponent,
  ],
})
export class OrganismsModule {}
