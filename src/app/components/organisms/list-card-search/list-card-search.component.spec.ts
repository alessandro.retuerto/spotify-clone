import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { ListCardSearchComponent } from './list-card-search.component';

describe('ListCardSearchComponent', () => {
  let component: ListCardSearchComponent;
  let fixture: ComponentFixture<ListCardSearchComponent>;
  let store: MockStore;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListCardSearchComponent],
      providers: [
        provideMockStore({
          initialState: {
            artist: {
              artist: [],
            },
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCardSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
