import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  loadArtist,
  loadedArtistId,
} from '../../../../../src/app/state/actions/artist.actions';
import { selectListArtist } from '../../../../../src/app/state/selectors/artist.selector';

@Component({
  selector: 'app-list-card-search',
  templateUrl: './list-card-search.component.html',
  styleUrls: ['./list-card-search.component.scss'],
})
export class ListCardSearchComponent implements OnInit {
  artist$: Observable<any> = new Observable();

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.store.dispatch(loadArtist());
    this.artist$ = this.store.select(selectListArtist);
    this.artist$.subscribe((data) => {});
    this.store.dispatch(loadedArtistId({ parameters: 'Linkin Park' }));
  }
}
