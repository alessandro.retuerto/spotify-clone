import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCardMusicComponent } from './list-card-music.component';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { itemMocks } from '../../../mocks/itemMocks';

import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../../../app-routing.module';
import { FigureComponent } from '../../atoms/figure/figure.component';
import { CardArtistComponent } from '../../molecules/card-artist/card-artist.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ListCardMusicComponent', () => {
  let component: ListCardMusicComponent;
  let fixture: ComponentFixture<ListCardMusicComponent>;
  let store: MockStore;
  let compilet: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ListCardMusicComponent,
        CardArtistComponent,
        FigureComponent,
      ],
      imports: [
        [RouterTestingModule.withRoutes(routes)],
        HttpClientTestingModule,
      ],
      providers: [
        provideMockStore({
          initialState: {
            item: {
              item: [],
            },
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCardMusicComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
    compilet = fixture.nativeElement;
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('se creo toda la lista de 20 elementos', () => {
    store.setState({
      item: {
        item: itemMocks.albums.items,
      },
    });
    store.refreshState();
    fixture.detectChanges();

    const list = fixture.debugElement.query(
      By.css('[data-testid="list-card"]')
    );
    expect(list.childNodes.length - 1).toBe(itemMocks.albums.items.length);
  });
});
