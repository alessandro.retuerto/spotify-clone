import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/Observable';
import { selectListItem } from '../../../state/selectors/item.selector';
import { AppState } from '../../../state/app.state';

@Component({
  selector: 'app-list-card-music',
  templateUrl: './list-card-music.component.html',
  styleUrls: ['./list-card-music.component.scss'],
})
export class ListCardMusicComponent implements OnInit {
  items$: Observable<any> = new Observable();

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.items$ = this.store.select(selectListItem);

    this.items$.subscribe((data) => {});
  }
}
