import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCardArtistComponent } from './list-card-artist.component';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { SpotifyService } from '../../../services/spotify.service';
import { CardArtistComponent } from '../../molecules/card-artist/card-artist.component';

describe('ListCardArtistComponent', () => {
  let component: ListCardArtistComponent;
  let fixture: ComponentFixture<ListCardArtistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListCardArtistComponent, CardArtistComponent],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        SpotifyService,
        provideMockStore({
          initialState: {
            artst: {
              artist: [],
            },
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCardArtistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
