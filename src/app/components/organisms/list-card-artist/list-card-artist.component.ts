import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { SpotifyService } from '../../../../../src/app/services/spotify.service';
import { AppState } from '../../../../../src/app/state/app.state';
import { selectListFavorite } from '../../../../../src/app/state/selectors/favorite.selector';

@Component({
  selector: 'app-list-card-artist',
  templateUrl: './list-card-artist.component.html',
  styleUrls: ['./list-card-artist.component.scss'],
})
export class ListCardArtistComponent implements OnInit {
  topTracks: any[] = [];
  artist: any = {};
  listFavorite: any;

  constructor(
    private rauter: ActivatedRoute,
    private spotify: SpotifyService,
    private store: Store<AppState>
  ) {
    this.rauter.params.subscribe((params: any) => {
      this.getArtist(params.id);
      this.getTopTracks(params.id);
    });
  }

  ngOnInit(): void {
    this.store.select(selectListFavorite).subscribe((data) => {
      this.listFavorite = data;
    });
  }

  getArtist(id: string) {
    this.spotify.getArtist(id).subscribe({
      next: (data: any) => {
        this.artist = data;
      },
      error: (error) => {
        console.log(error);
      },
    });
  }
  getTopTracks(id: string) {
    this.spotify.getTopTracks(id).subscribe({
      next: (data: any) => {
        this.topTracks = data.tracks;
      },
      error: (error) => {
        console.log(error);
      },
    });
  }
}
