import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { ListCardFavoriteComponent } from './list-card-favorite.component';
import { SpotifyService } from '../../../services/spotify.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { favorite } from '../../../mocks/favoriteMock';
import { By } from '@angular/platform-browser';
import { FigureComponent } from '../../atoms/figure/figure.component';
import { CardArtistComponent } from '../../molecules/card-artist/card-artist.component';

describe('ListCardFavoriteComponent', () => {
  let component: ListCardFavoriteComponent;
  let fixture: ComponentFixture<ListCardFavoriteComponent>;
  let store: MockStore;
  let compilet: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ListCardFavoriteComponent,
        FigureComponent,
        CardArtistComponent,
      ],
      imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule],
      providers: [
        provideMockStore({
          initialState: {
            favorite: {
              favorite: [],
            },
          },
        }),
        SpotifyService,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCardFavoriteComponent);
    component = fixture.componentInstance;
    store = TestBed.get<any>(MockStore);
    fixture.detectChanges();
    compilet = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  test('se creo toda la lista de 3 elementos en favoritos', () => {
    component.ngOnInit();
    store.setState({
      favorite: {
        favorite: favorite,
      },
    });
    fixture.detectChanges();
    store.refreshState();
    const list = fixture.debugElement.query(
      By.css('[data-testid="list-cardfavorite"]')
    );
    expect(list.childNodes.length - 1).toBe(favorite.length);
  });
});
