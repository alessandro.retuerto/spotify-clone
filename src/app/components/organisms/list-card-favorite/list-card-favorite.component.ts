import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Favorite } from '../../../../../src/app/interface/favorite.interface';
import { SpotifyService } from '../../../../../src/app/services/spotify.service';
import { removeFavorite } from '../../../../../src/app/state/actions/favorite.actions';
import { AppState } from '../../../../../src/app/state/app.state';
import { selectListFavorite } from '../../../../../src/app/state/selectors/favorite.selector';

@Component({
  selector: 'app-list-card-favorite',
  templateUrl: './list-card-favorite.component.html',
  styleUrls: ['./list-card-favorite.component.scss'],
})
export class ListCardFavoriteComponent implements OnInit {
  listFavorite: any;
  hasFavorites: boolean = false;
  isSelectMusic: boolean = false;
  constructor(
    private store: Store<AppState>,
    private spotify: SpotifyService
  ) {}

  ngOnInit(): void {
    this.isSelectMusic = this.spotify.isSelectMusic;
    this.store.select(selectListFavorite).subscribe((data) => {
      this.listFavorite = data;

      this.validateFavorite();
    });
  }
  removeFavorite(favorite: Favorite) {
    this.store.dispatch(removeFavorite({ favorite: favorite }));
    this.validateFavorite();
  }
  validateFavorite() {
    if (this.listFavorite.length > 0) {
      this.hasFavorites = false;
    } else {
      this.hasFavorites = true;
    }
  }
}
