import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../../../../src/app/services/spotify.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  isControls: boolean = true;
  constructor(private spotify: SpotifyService) {}

  ngOnInit(): void {
    this.isControls = this.spotify.isSelectMusic;
  }
}
