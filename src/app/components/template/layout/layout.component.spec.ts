import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutComponent } from './layout.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HeaderLateralComponent } from '../../molecules/header-lateral/header-lateral.component';
import { HeaderComponent } from '../../molecules/header/header.component';
import { FooterComponent } from '../../molecules/footer/footer.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SubtitleComponent } from '../../atoms/subtitle/subtitle.component';
import { FigureComponent } from '../../atoms/figure/figure.component';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { SpotifyService } from '../../../../../src/app/services/spotify.service';

describe('LayoutComponent', () => {
  let component: LayoutComponent;
  let fixture: ComponentFixture<LayoutComponent>;
  let store: MockStore;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        LayoutComponent,
        HeaderLateralComponent,
        HeaderComponent,
        FooterComponent,
        SubtitleComponent,
        FigureComponent,
      ],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        provideMockStore({
          initialState: {
            favorite: {
              favorite: [],
            },
          },
        }),
        SpotifyService,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
