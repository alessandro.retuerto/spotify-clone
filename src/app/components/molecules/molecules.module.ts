import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AtomsModule } from '../atoms/atoms.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderLateralComponent } from './header-lateral/header-lateral.component';
import { CardArtistComponent } from './card-artist/card-artist.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    HeaderLateralComponent,
    CardArtistComponent,
  ],
  imports: [CommonModule, AtomsModule],
  exports: [
    HeaderComponent,
    FooterComponent,

    HeaderLateralComponent,
    CardArtistComponent,
  ],
})
export class MoleculesModule {}
