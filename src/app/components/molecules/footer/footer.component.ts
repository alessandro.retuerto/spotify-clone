import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  subTitles = [
    { icon: 'fa-solid fa-house', title: 'Home', route: '/home' },
    { icon: 'fa-solid fa-magnifying-glass', title: 'Search', route: '/search' },

    { icon: 'fa-solid fa-heart', title: 'Favorites', route: '/favorites' },
  ];
  constructor() {}

  ngOnInit(): void {}
}
