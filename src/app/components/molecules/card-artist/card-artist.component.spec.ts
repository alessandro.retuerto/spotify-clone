import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { CardArtistComponent } from './card-artist.component';
import { FigureComponent } from '../../atoms/figure/figure.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('CardArtistComponent', () => {
  let component: CardArtistComponent;
  let fixture: ComponentFixture<CardArtistComponent>;
  let store: MockStore;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [CardArtistComponent, FigureComponent],
      providers: [
        provideMockStore({
          initialState: {
            artist: {
              artist: [],
            },
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardArtistComponent);
    compiled = fixture.nativeElement;
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
