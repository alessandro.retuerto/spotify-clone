import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { Store } from '@ngrx/store';
import { Favorite } from '../../../../../src/app/interface/favorite.interface';
import { SpotifyService } from '../../../../../src/app/services/spotify.service';
import {
  addFavorites,
  removeFavorite,
} from '../../../../../src/app/state/actions/favorite.actions';
import { AppState } from '../../../../../src/app/state/app.state';
import { selectListFavorite } from '../../../../../src/app/state/selectors/favorite.selector';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-artist',
  templateUrl: './card-artist.component.html',
  styleUrls: ['./card-artist.component.scss'],
})
export class CardArtistComponent implements OnInit {
  @Input()
  item!: any;
  @Input()
  music!: any;
  // fusion music tototo
  @Input()
  image!: string;
  @Input()
  titleTest!: string;
  @Input()
  albumTest!: string;
  @Input()
  showHeart: boolean = false;
  @Input()
  width: string = '12rem';
  @Input()
  checkViewArtist: boolean = false;
  @Input()
  checkPlayMusic: boolean = false;
  @Input()
  checkFavorite: boolean = true;

  @Output()
  showControls = new EventEmitter<boolean>();
  isSelectMusic: boolean = false;
  listFavorite: any;

  constructor(
    private spotify: SpotifyService,
    private store: Store<AppState>,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.isSelectMusic = this.spotify.isSelectMusic;
    this.store.select(selectListFavorite).subscribe((data) => {
      this.listFavorite = data;
    });
  }

  playMusic(preview: any) {
    this.showControls.emit(true);
    this.spotify.audio.src = preview.preview_url;
    this.spotify.isSelectMusic = true;

    if (this.spotify.audio.played) {
      this.spotify.audio.play();
    } else {
      this.spotify.audio.pause();
    }

    this.spotify.musicInit = {
      image: preview.album.images[0].url,
      musicName: preview.name,
      album: preview.album.name,
    };
  }
  favorite() {
    let obj: Favorite = {
      image: this.item.album.images[0].url,
      song: this.item.name,
      album: this.item.album.name,
      id: this.item.id,
      isSelect: true,
    };
    const select = this.listFavorite.find(
      (test: any) => test.id == this.item.id
    );

    if (select) {
      this.store.dispatch(removeFavorite({ favorite: obj }));
      return;
    }
    this.store.dispatch(addFavorites({ favorite: obj }));
  }

  isFavorite(item: any) {
    return this.listFavorite.find((test: any) => test.id == item.id);
  }

  viewArtist() {
    let artistId;
    if (this.music.type === 'artist') {
      artistId = this.music.id;
    } else {
      artistId = this.music.artists[0].id;
    }
    this.router.navigate(['/artist', artistId]);
  }
}
