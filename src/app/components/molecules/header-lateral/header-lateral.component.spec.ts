import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderLateralComponent } from './header-lateral.component';
import { SubtitleComponent } from '../../atoms/subtitle/subtitle.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('HeaderLateralComponent', () => {
  let component: HeaderLateralComponent;
  let fixture: ComponentFixture<HeaderLateralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderLateralComponent, SubtitleComponent],
      imports: [RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderLateralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
