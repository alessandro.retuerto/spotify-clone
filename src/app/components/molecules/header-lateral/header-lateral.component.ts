import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-lateral',
  templateUrl: './header-lateral.component.html',
  styleUrls: ['./header-lateral.component.scss'],
})
export class HeaderLateralComponent implements OnInit {
  subTitles = [
    { icon: 'fa-solid fa-house', title: 'Home', route: '/home' },
    { icon: 'fa-solid fa-magnifying-glass', title: 'Search', route: '/search' },

    { icon: 'fa-solid fa-heart', title: 'Favorites', route: '/favorites' },
  ];
  constructor() {}

  ngOnInit(): void {}
}
