import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../../../../../src/app/interface/user.interface';
import { UserService } from '../../../../../src/app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  fecha = new Date();
  hora = this.fecha.getHours();
  text = 'Good Morning';
  isShowLogOut: boolean = false;
  user: string = '';

  constructor(
    private router: Router,
    private cookieService: CookieService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.userService.getUser().subscribe((data: User) => {
      this.user = data.display_name.split(' ')[0];
    });
    this.showMessage();
  }

  logOut() {
    this.cookieService.delete('token');
    this.router.navigate(['/login']);
  }

  showAcount() {
    this.isShowLogOut = !this.isShowLogOut;
  }
  showMessage() {
    if (this.hora >= 0 && this.hora < 12) {
      this.text = 'Good Morning ';
    }

    if (this.hora >= 12 && this.hora < 18) {
      this.text = `Good Afternoon`;
    }

    if (this.hora >= 18 && this.hora < 24) {
      this.text = 'Good Night';
    }
  }
}
