import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubtitleComponent } from './subtitle.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('SubtitleComponent', () => {
  let component: SubtitleComponent;
  let fixture: ComponentFixture<SubtitleComponent>;
  let compiled: HTMLElement;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SubtitleComponent],
      imports: [RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubtitleComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Se renderiza todos los elementos la etiqueta a ', () => {
    component.title = 'home';
    component.icon = 'fa fa-home';
    component.route = '/home';
    const title = compiled.querySelector('.a-subtitle__title');
    const icon = compiled.querySelector('i');
    const route = compiled.querySelector('a');

    fixture.detectChanges();
    expect(title?.innerHTML).toEqual('home');
    expect(icon?.className).toEqual('fa fa-home');
    expect(route?.href.substring(16)).toEqual('/home');
  });
});
