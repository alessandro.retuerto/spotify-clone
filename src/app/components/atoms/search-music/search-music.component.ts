import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, filter } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import {
  loadArtist,
  loadedArtistId,
} from '../../../../../src/app/state/actions/artist.actions';

@Component({
  selector: 'app-search-music',
  templateUrl: './search-music.component.html',
  styleUrls: ['./search-music.component.scss'],
})
export class SearchMusicComponent implements OnInit {
  @Output()
  listMusic = new EventEmitter<any>();

  searchControl: FormControl;

  constructor(private store: Store) {
    this.searchControl = new FormControl();

    this.searchControl.valueChanges.pipe(debounceTime(600)).subscribe((res) => {
      if (res === '') {
        this.search('Linkin Park');
      } else {
        this.search(res);
      }
    });
  }

  ngOnInit(): void {}

  search(term: string) {
    this.store.dispatch(loadArtist());
    this.store.dispatch(loadedArtistId({ parameters: term }));
  }
}
