import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchMusicComponent } from './search-music.component';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('SearchMusicComponent', () => {
  let component: SearchMusicComponent;
  let fixture: ComponentFixture<SearchMusicComponent>;
  let store: MockStore;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchMusicComponent],
      imports: [RouterTestingModule, ReactiveFormsModule],
      providers: [
        provideMockStore({
          initialState: {
            artist: {
              artist: [],
            },
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMusicComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
