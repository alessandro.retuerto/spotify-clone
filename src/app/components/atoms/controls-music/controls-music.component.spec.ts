import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlsMusicComponent } from './controls-music.component';

import { SpotifyService } from '../../../services/spotify.service';
import { FigureComponent } from '../figure/figure.component';
import { provideMockStore } from '@ngrx/store/testing';

describe('ControlsMusicComponent', () => {
  let component: ControlsMusicComponent;
  let fixture: ComponentFixture<ControlsMusicComponent>;
  let spotfiServ: SpotifyService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ControlsMusicComponent, FigureComponent],
      imports: [HttpClientModule],
      providers: [
        SpotifyService,
        provideMockStore({
          initialState: {},
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlsMusicComponent);
    component = fixture.componentInstance;
    spotfiServ = TestBed.inject(SpotifyService);
    fixture.detectChanges();
  });
  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    spotfiServ.musicInit = {
      image: 'https://i.scdn.co/image/ab67616d00001e02e2f039481babe23658fc719a',
      musicName: 'In the End',
      album: 'Hybrid Theory (Bonus Edition)',
    };

    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
