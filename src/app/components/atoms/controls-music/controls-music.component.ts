import { SpotifyService } from '../../../../../src/app/services/spotify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-controls-music',
  templateUrl: './controls-music.component.html',
  styleUrls: ['./controls-music.component.scss'],
})
export class ControlsMusicComponent implements OnInit {
  image: string = '';
  musicName: string = '';
  album: string = 'Hybrid Theory (Bonus Edition)';
  isPlaying: boolean = true;

  constructor(private spotify: SpotifyService) {}

  ngDoCheck(): void {
    this.image = this.spotify.musicInit.image;
    this.musicName = this.spotify.musicInit.musicName;
    this.album = this.spotify.musicInit.album;
  }
  ngOnInit(): void {}

  playMusic() {
    if (this.spotify.audio.paused) {
      this.spotify.audio.play();
    } else {
      this.spotify.audio.pause();
    }
    this.isPlaying = !this.isPlaying;
  }

  classIcon() {
    return this.isPlaying ? 'fa-solid fa-play' : 'fa-solid fa-pause';
  }
}
