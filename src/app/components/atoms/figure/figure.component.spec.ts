import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FigureComponent } from './figure.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideMockStore } from '@ngrx/store/testing';

describe('FigureComponent', () => {
  let component: FigureComponent;
  let fixture: ComponentFixture<FigureComponent>;
  let compiled: HTMLElement;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FigureComponent],
      imports: [HttpClientTestingModule],
      providers: [
        provideMockStore({
          initialState: {
            favorite: {
              favorite: [],
            },
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FigureComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Se renderiza la imagen ', () => {
    component.image =
      'https://i.scdn.co/image/ab67616d0000b273d3fdbbbfeeffc9a02c6308d1';

    const img = compiled.querySelector('img');

    fixture.detectChanges();
    expect(img?.src).toEqual(
      'https://i.scdn.co/image/ab67616d0000b273d3fdbbbfeeffc9a02c6308d1'
    );
  });
});
