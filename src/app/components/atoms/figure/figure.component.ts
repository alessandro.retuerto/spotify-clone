import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Favorite } from '../../../../../src/app/interface/favorite.interface';
import {
  addFavorites,
  removeFavorite,
} from '../../../../../src/app/state/actions/favorite.actions';
import { AppState } from '../../../../../src/app/state/app.state';
import { selectListFavorite } from '../../../../../src/app/state/selectors/favorite.selector';

@Component({
  selector: 'app-figure',
  templateUrl: './figure.component.html',
  styleUrls: ['./figure.component.scss'],
})
export class FigureComponent implements OnInit {
  @Input()
  image!: string;
  @Input()
  item!: any;
  @Input()
  showHeart!: boolean;
  @Input()
  width: string = '12rem';
  @Input()
  marginRigth: string = '';

  @Input()
  checkFavorite: boolean = true;
  listFavorite: any;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.select(selectListFavorite).subscribe((data) => {
      this.listFavorite = data;
    });
  }

  favorite() {
    let obj: Favorite = {
      image: this.item.album.images[0].url,
      song: this.item.name,
      album: this.item.album.name,
      id: this.item.id,
      isSelect: true,
    };
    const select = this.listFavorite.find(
      (test: any) => test.id == this.item.id
    );

    if (select) {
      this.store.dispatch(removeFavorite({ favorite: obj }));
      return;
    }
    this.store.dispatch(addFavorites({ favorite: obj }));
  }

  isFavorite(item: any) {
    return this.listFavorite.find((test: any) => test.id == item.id);
  }

  styleObject() {
    return {
      width: this.width,
    };
  }
}
