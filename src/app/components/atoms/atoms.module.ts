import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LogoComponent } from './logo/logo.component';
import { SubtitleComponent } from './subtitle/subtitle.component';
import { AppRoutingModule } from '../../../../src/app/app-routing.module';
import { SearchMusicComponent } from './search-music/search-music.component';
import { ControlsMusicComponent } from './controls-music/controls-music.component';
import { SharedModule } from '../shared/shared.module';
import { FigureComponent } from './figure/figure.component';

@NgModule({
  declarations: [
    LogoComponent,
    SubtitleComponent,
    SearchMusicComponent,
    ControlsMusicComponent,
    FigureComponent,
  ],
  imports: [CommonModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
  exports: [
    LogoComponent,
    SubtitleComponent,
    RouterModule,
    SearchMusicComponent,
    ControlsMusicComponent,
    SharedModule,
    FigureComponent,
  ],
})
export class AtomsModule {}
